package com.mochat.Mock;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import static com.mochat.Sync.MessageReceiver.ACTION_MESSAGE;
import static com.mochat.Sync.MessageReceiver.EXTRA_MESSAGE;
import static com.mochat.Sync.MessageReceiver.EXTRA_PHONE;

public class MockInboxIntentService extends IntentService {


    public MockInboxIntentService() {
        super("MockInboxIntentService");
    }

    public static void startActionSendMessage(Context context) {
        Intent intent = new Intent(context, MockInboxIntentService.class);
        intent.setAction(ACTION_MESSAGE);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_MESSAGE.equals(action)) {
                handleActionSend();
            }
        }
    }

    private void handleActionSend() {
        Log.d("MSG", "send from service");
        Intent i = new Intent(ACTION_MESSAGE);
        i.putExtra(EXTRA_PHONE, "+375292281480");
        i.putExtra(EXTRA_MESSAGE, "MOCK INCOME FROM INTENTSERVICE");
        sendBroadcast(i);
    }
}
