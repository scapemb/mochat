package com.mochat.Mock;

import com.mochat.Models.Contact;
import com.mochat.Utils.AppDatabase;

import java.util.ArrayList;
import java.util.List;

public class MockContactsContent {
    private static final int COUNT = 1;
    private static List<Contact> sContacts = new ArrayList<>();

    public static void fillDb(AppDatabase database){
        for (int i = 0; i < COUNT; i++) {
            addItem(createContactItem(i));
        }

        database.getContactDao().insertAll(sContacts.toArray(new Contact[sContacts.size()]));
    }

    private static void addItem(Contact contact) {
        sContacts.add(contact);
    }

    private static Contact createContactItem(int position) {
        return new Contact("+375256257514", makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }
}
