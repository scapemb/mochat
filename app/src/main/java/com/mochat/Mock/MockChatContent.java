package com.mochat.Mock;

import com.mochat.Models.Chat;
import com.mochat.Models.Message;
import com.mochat.Utils.AppDatabase;

import java.util.ArrayList;
import java.util.List;

public class MockChatContent {

    public static final List<Chat> sChats = new ArrayList<>();
    private static final int CHAT_COUNT = 1;
    private static final int MSG_COUNT = 0;

    public static void fillDb(AppDatabase database){
        for (int i = 0; i < CHAT_COUNT; i++) {
            createChatItem(database, i);
        }
    }

    private static void addItem(Chat chat) {
        sChats.add(chat);
    }

    private static void createChatItem(AppDatabase database, int position) {
        String phoneRecipient = "+375256257514";

        Chat chat = new Chat( phoneRecipient);
        database.getChatDao().insert(chat);

/*        for (int i = 0; i <= MSG_COUNT; i++) {
            createMessageItem(database, phoneRecipient, (position * 51) + i);
        }
        database.getChatDao().insertAll(sChats.toArray(new Chat[sChats.size()]));*/
    }

    private static void createMessageItem(AppDatabase database, String recipientPhone, int position) {
        database.getChatDao().insert(new Message(recipientPhone, getRandomBoolean(), makeDetails(position)));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Message : ").append(position);

        builder.append("\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ");

        return builder.toString();
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }
}
