package com.mochat.Sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mochat.MochatApp;
import com.mochat.Models.Chat;
import com.mochat.Models.Message;
import com.mochat.Utils.AppDatabase;
import com.mochat.Utils.NotificationUtil;

public class MessageReceiver extends BroadcastReceiver {
    public static final String ACTION_MESSAGE = "com.mochat.action.ACTION_MESSAGE";
    public static final String EXTRA_PHONE = "com.mochat.extra.PHONE";
    public static final String EXTRA_MESSAGE = "com.mochat.extra.MESSAGE";

    private AppDatabase mDb;

    @Override
    public void onReceive(Context context, Intent intent) {
        String phone = intent.getStringExtra(EXTRA_PHONE);
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        writeMessageToDb(context, new Message(phone, message));

        NotificationUtil.buildNotification(context, message);
    }

    private void writeMessageToDb(Context context, Message message){
        if(message.isOutgoing()) return;

        MochatApp mApp = ((MochatApp)context.getApplicationContext());
        mDb = mApp.getDb();
        Chat chat = mDb.getChatDao().getChatForPhone(message.getRecipientPhone());
        if(null == chat){
            mDb.getChatDao().insert(new Chat(message.getRecipientPhone()));
        }
        Log.d("WRITE", "write in receiver");
        mDb.getChatDao().insert(message);
    }
}
