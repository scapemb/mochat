package com.mochat.Sync;

import android.content.Context;

import com.mochat.MochatApp;
import com.mochat.Models.Message;
import com.mochat.Utils.AppDatabase;
import com.mochat.Utils.CredsUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by PICA on 23.10.2017.
 */

public class MessageSender {

    public static final String API_KEY = "key=AIzaSyAmNMJ4KrNIneBbP4OyvVs_mbfxIMnxwIc";

    private AppDatabase mDb;
    public void send(final Context context, final Message message) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject jPayload = new JSONObject();
                JSONObject jNotification = new JSONObject();
                JSONObject jData = new JSONObject();
                try {

                    jNotification.put("title", message.getRecipientPhone());                    //use Phone number as title (to)
                    jNotification.put("body", message.getMessage());                            //use Message as body
                    jNotification.put("tag", CredsUtil.getInstance().getCurrentPhone());        //use Current phone as tag (from)
                    jPayload.put("to", "/topics/mochat");
                    jPayload.put("priority", "high");
                    jPayload.put("notification", jNotification);
                    jPayload.put("data", jData);

                    URL url = new URL("https://fcm.googleapis.com/fcm/send");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Authorization", API_KEY);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoOutput(true);

                    // Send FCM message content.
                    OutputStream outputStream = conn.getOutputStream();
                    outputStream.write(jPayload.toString().getBytes());

                    // Read FCM response.
                    InputStream inputStream = conn.getInputStream();
                    final String resp = convertStreamToString(inputStream);

                    writeMessageToDb(context, message);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();

        }

    private static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }

    private void writeMessageToDb(Context context, Message message){
        MochatApp mApp = ((MochatApp)context.getApplicationContext());
        mDb = mApp.getDb();
        mDb.getChatDao().insert(message);
    }
}
