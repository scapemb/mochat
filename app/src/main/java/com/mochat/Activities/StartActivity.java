package com.mochat.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mochat.DAO.ChatDAO;
import com.mochat.DAO.ContactDAO;
import com.mochat.MochatApp;
import com.mochat.Mock.MockChatContent;
import com.mochat.Mock.MockContactsContent;
import com.mochat.Utils.AppDatabase;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startMainActivity();
    }

    private void startMainActivity(){
        //mockDb();

        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);

        finish();
    }

    private void mockDb(){
        mockContacts();
    }

    private void mockContacts(){
        AppDatabase db = ((MochatApp)getApplication()).getDb();
        if(null != db){
            ContactDAO contactDAO = db.getContactDao();
            if(contactDAO.getAllContacts().size() == 0){
                MockContactsContent.fillDb(db);
            }
            ChatDAO chatDAO = db.getChatDao();
            if(chatDAO.getAllChats().size() == 0){
                MockChatContent.fillDb(db);
            }
        }
    }
}
