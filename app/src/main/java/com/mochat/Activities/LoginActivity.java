package com.mochat.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mochat.R;
import com.mochat.Utils.CredsUtil;

import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, OnCompleteListener<AuthResult> {//} implements LoaderCallbacks<Cursor> {
    private static final String TAG = "LOGIN";

    private static final String DUMMY_CREDENTIALS ="+375293704923";
    private String mInputPhone;

    private AutoCompleteTextView mPhoneEdit;
    private ProgressBar mProgress;
    private Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPhoneEdit = findViewById(R.id.edit_phone);
        mLoginButton = findViewById(R.id.button_login);
        mProgress = findViewById(R.id.progress_login);
        setLoadingVisibility(false);
        mLoginButton.setOnClickListener(this);

        //TODO: for testing purposes
        mPhoneEdit.setText(DUMMY_CREDENTIALS);
        //verifyLogin();
    }

    private void setLoadingVisibility(boolean isLoading){
        mProgress.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
    }

    private void verifyLogin(){

        mInputPhone = mPhoneEdit.getText().toString();
        if(!mInputPhone.isEmpty()){

            setLoadingVisibility(true);
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    mInputPhone,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
        }
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verificaiton without
            //     user action.
            Log.d(TAG, "onVerificationCompleted:" + credential);

            FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(LoginActivity.this);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Log.w(TAG, "onVerificationFailed", e);
            setLoadingVisibility(false);
            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(String verificationId,
                PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Log.d(TAG, "onCodeSent:" + verificationId);
            //setLoadingVisibility(false);
            // Save verification ID and resending token so we can use them later
            //mVerificationId = verificationId;
            //mResendToken = token;

            // ...
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_login:{
                verifyLogin();
            }
        }
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        setLoadingVisibility(false);

        CredsUtil.getInstance().setCurrentPhone(mInputPhone);
        FirebaseMessaging.getInstance().subscribeToTopic("mochat");

        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}

