package com.mochat.Activities;

import android.arch.persistence.room.InvalidationTracker;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mochat.Adapters.ChatRecyclerViewAdapter;
import com.mochat.MochatApp;
import com.mochat.Models.Chat;
import com.mochat.Models.Contact;
import com.mochat.Models.Message;
import com.mochat.R;
import com.mochat.Sync.MessageReceiver;
import com.mochat.Sync.MessageSender;
import com.mochat.Utils.AppDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private AppDatabase mDb;

    MessageReceiver mReceiver = null;
    boolean isReceiverRegistered = false;

    private ChatRecyclerViewAdapter mAdapter;

    private RecyclerView mChatRecycler;
    private EditText mInputEdit;
    private Button mSendButton;

    public static final String CHAT_PHONE = "arg_chat_phone";

    private String recipientPhone;

    private InvalidationTracker.Observer mObserver = new InvalidationTracker.Observer("messages") {
        @Override
        public void onInvalidated(@NonNull Set<String> set) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateChat();
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mReceiver = new MessageReceiver();

        getExtras();
        initView();
    }

    private void getExtras(){
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            recipientPhone = extras.getString(CHAT_PHONE);
        }
    }

    private void initView(){
        View coreView = findViewById(R.id.core_view);

        mChatRecycler = findViewById(R.id.chat);
        mInputEdit = findViewById(R.id.edit_input);
        mSendButton = findViewById(R.id.button_send);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initChatAdapter();
        loadChat();
        initInput();

        registerMessageReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterMessageReceiver();
    }

    private void registerMessageReceiver(){
        if(!isReceiverRegistered){
            //registerReceiver(mReceiver, new IntentFilter(MessageReceiver.ACTION_MESSAGE));
            mDb.getInvalidationTracker().addObserver(mObserver);
            isReceiverRegistered = true;
        }
    }
    private void unregisterMessageReceiver(){
        if(isReceiverRegistered){
            //unregisterReceiver(mReceiver);
            mDb.getInvalidationTracker().removeObserver(mObserver);
            isReceiverRegistered = false;
        }
    }

    private void initChatAdapter(){
        mChatRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        mAdapter = new ChatRecyclerViewAdapter(this);
        mChatRecycler.setAdapter(mAdapter);
    }

    private Contact getRecipientContact(){
        Contact recipient = mDb.getContactDao().getContactWithPhone(recipientPhone);
        //TODO: set title with name
        return recipient;
    }

    private void initInput(){
        mInputEdit.addTextChangedListener(mInputTextChangedWatcher);
        mSendButton.setOnClickListener(this);
    }

    private void initDb(){
        mDb = ((MochatApp)getApplication()).getDb();
    }

    private void loadChat(){
        initDb();

        updateChat();
    }

    private void sendMessage(){
        new MessageSender().send(this, new Message(recipientPhone, true, getMessage()));
        clearInput();
    }

    private void updateChat(){
        if(null != recipientPhone && !recipientPhone.isEmpty()) {
            Chat chat = mDb.getChatDao().getChatForPhone(recipientPhone);
            Contact recipient = getRecipientContact();
            if(null != chat ) {
                List<Message> messages = mDb.getChatDao().getInnerAllMessagesForChat(chat.getRecipientPhone());
                ArrayList<Message> reverse = new ArrayList<>(messages);
                Collections.reverse(reverse);
                mAdapter.setMessages(reverse);
                if(null != recipient){
                    mAdapter.setContact(recipient);
                }

                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private @Nullable String getMessage(){
        if(null != mInputEdit){
            return mInputEdit.getText().toString();
        }
        return null;
    }

    private void clearInput(){
        if(null != mInputEdit){
            mInputEdit.setText("");
        }
    }

    private TextWatcher mInputTextChangedWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_send:{
                sendMessage();
                break;
            }
        }
    }


    final static float STEP = 200;
    float mRatio = 1.0f;
    int mBaseDist;
    float mBaseRatio;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getPointerCount() == 2) {
            int action = event.getAction();
            int pureaction = action & MotionEvent.ACTION_MASK;
            if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                mBaseDist = getDistance(event);
                mBaseRatio = mRatio;
            } else {
                float delta = (getDistance(event) - mBaseDist) / STEP;
                float multi = (float) Math.pow(2, delta);
                mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                mAdapter.setTextSize(mRatio);
            }
            return false;
        }
        else return super.dispatchTouchEvent(event);
    }

    int getDistance(MotionEvent event) {
        int dx = (int) (event.getX(0) - event.getX(1));
        int dy = (int) (event.getY(0) - event.getY(1));
        return (int) (Math.sqrt(dx * dx + dy * dy));
    }
}
