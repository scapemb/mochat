package com.mochat.Activities;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mochat.Fragments.ContactInfoFragment;
import com.mochat.Fragments.ContactsFragment;
import com.mochat.Fragments.EditContactFragment;
import com.mochat.Fragments.EditProfileFragment;
import com.mochat.MochatApp;
import com.mochat.Models.Contact;
import com.mochat.R;
import com.mochat.Utils.AppDatabase;
import com.mochat.Utils.ContactOptionsDialogFragment;

public class MainActivity extends AppCompatActivity implements LifecycleOwner, View.OnClickListener, ContactsFragment.OnContactsListFragmentInteractionListener, ContactOptionsDialogFragment.OnContactOptionsSelected {

    public static final String ARG_SHARED_VIEW = "arg_shared_view";
    private FloatingActionButton mFabAction;
    private AppDatabase mDb;

    private ContactOptionsDialogFragment mContactOptionsDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFabAction = findViewById(R.id.fab_action);
        mFabAction.setOnClickListener(this);

        initDb();

        mContactOptionsDialogFragment = new ContactOptionsDialogFragment();
        mContactOptionsDialogFragment.setOptionsSelectedCallback(this);

        showContactList();
    }

    private void initDb(){
        mDb = ((MochatApp)getApplication()).getDb();
    }

    private void showContactList() {
        ContactsFragment fragment = ContactsFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_fragment, fragment)
                .addToBackStack("list");

        transaction.commit();
    }

    private void showContactInfo(Contact contact) {
        ContactInfoFragment fragment = ContactInfoFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putString(ContactInfoFragment.ARG_CONTACT_PHONE, contact.getPhoneMain());
        fragment.setArguments(bundle);


        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_fragment, fragment)
                .addToBackStack("info");

        transaction.commit();
    }

    private void addContact(){
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container_fragment);
        if(f instanceof EditContactFragment){
            ((EditContactFragment) f).saveContact();
            onBackPressed();
        }else {
            EditContactFragment fragment = EditContactFragment.newInstance();


            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container_fragment, fragment)
                    .addToBackStack("add");

            transaction.commit();
        }
    }

    private void editCurrentProfile(){
        EditProfileFragment fragment = EditProfileFragment.newInstance();


        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_fragment, fragment)
                .addToBackStack("edit");

        transaction.commit();

    }

    private void showChat(Contact contact) {
        Intent i = new Intent(this, ChatActivity.class);
        i.putExtra(ChatActivity.CHAT_PHONE, contact.getPhoneMain());
        startActivity(i);
    }

    private void clearChat(Contact contact){
        mDb.getChatDao().deleteAllMessagesForChat(contact.getPhoneMain());
    }

    private void deleteContact(Contact contact){
        clearChat(contact);
        mDb.getContactDao().delete(contact);
    }

    @Override
    public void onContactClick(Contact item, View sharedView) {
        showChat(item);
    }

    @Override
    public void onContactLongClick(Contact item, View sharedView) {
        //showContactInfo(item);
        //editCurrentProfile();
        mContactOptionsDialogFragment.setContact(item);
        mContactOptionsDialogFragment.show(getSupportFragmentManager(), "options");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab_action:{
                addContact();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1){
            getSupportFragmentManager().popBackStackImmediate();
        }
        else{
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_settings:{
                editCurrentProfile();
                break;
            }
        }
        return true;
    }


    @Override
    public void onOpenInfoSelected(Contact contact) {
        showContactInfo(contact);
    }

    @Override
    public void onEditInfoSelected(Contact contact) {

    }

    @Override
    public void onClearChatSelected(Contact contact) {
        clearChat(contact);
    }

    @Override
    public void onDeleteSelected(Contact contact) {
        deleteContact(contact);
    }
}
