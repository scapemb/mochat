package com.mochat.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mochat.Adapters.ContactInfoRecyclerViewAdapter;
import com.mochat.MochatApp;
import com.mochat.Models.Contact;
import com.mochat.R;
import com.mochat.Utils.AppDatabase;

import java.util.List;

public class ContactInfoFragment extends Fragment {

    public final static String ARG_CONTACT_PHONE = "arg_contact";
    private AppDatabase mDb;

    private String mContactPhone;
    private  ContactInfoRecyclerViewAdapter mAdapter;

    public ContactInfoFragment() {
    }

    @SuppressWarnings("unused")
    public static ContactInfoFragment newInstance() {
        return new ContactInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mContactPhone = getArguments().getString(ARG_CONTACT_PHONE);
        }

        initDb();
    }

    private void initDb(){
        mDb = ((MochatApp)getActivity().getApplication()).getDb();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_info, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

            List<Contact> contacts = mDb.getContactDao().getAllContacts();
            mAdapter = new ContactInfoRecyclerViewAdapter(getContext(), contacts);

            recyclerView.setAdapter(mAdapter);

            PagerSnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(recyclerView);

            for( int i = 0; i < contacts.size(); i++){
                if(contacts.get(i).getPhoneMain().equals(mContactPhone)){
                    recyclerView.scrollToPosition(i);
                    break;
                }
            }
        }
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
