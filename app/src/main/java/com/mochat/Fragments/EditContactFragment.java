package com.mochat.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.mochat.MochatApp;
import com.mochat.Models.Chat;
import com.mochat.Models.Contact;
import com.mochat.R;
import com.mochat.Utils.AppDatabase;
import com.squareup.picasso.Picasso;

public class EditContactFragment extends Fragment {

    protected AppDatabase mDb;

    protected EditText mEditName;
    protected EditText mEditPhone;
    protected ImageView mImageAvatar;
    protected Button mButtonPhotoChange;

    protected String mPhoneNumber = "";

    public EditContactFragment() {
    }

    public static EditContactFragment newInstance() {
        EditContactFragment fragment = new EditContactFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDb();
    }

    private void initDb(){
        mDb = ((MochatApp)getActivity().getApplication()).getDb();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_contact, container, false);

        mEditName = v.findViewById(R.id.edit_name);
        mEditPhone = v.findViewById(R.id.edit_phone);
        mImageAvatar = v.findViewById(R.id.avatar);
        mButtonPhotoChange = v.findViewById(R.id.button_photo_change);
        mButtonPhotoChange.setVisibility(View.GONE);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        setAvatar();
    }

    protected void setAvatar(){
        Picasso.with(getContext())
                .load(String.format(
                        MochatApp.BASE_URL,
                        mPhoneNumber.replace("+", "")))
                .placeholder(R.drawable.placeholder_avatar)
                .into(mImageAvatar);
    }

    public void saveContact(){
        String name = mEditName.getText().toString();
        String phone = mEditPhone.getText().toString();

        if(!name.isEmpty() && !phone.isEmpty()){
            mDb.getContactDao().insert(createContact(name, phone));
            Chat chat = mDb.getChatDao().getChatForPhone(phone);
            if(null == chat){
                mDb.getChatDao().insert(new Chat(phone));
            }
        }
    }

    private Contact createContact(String name, String phone) {
        return new Contact(phone, name);
    }
}
