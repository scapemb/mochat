package com.mochat.Fragments;

import android.Manifest;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mochat.R;
import com.mochat.Utils.CredsUtil;

import gun0912.tedbottompicker.TedBottomPicker;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class EditProfileFragment extends EditContactFragment implements View.OnClickListener {

    public EditProfileFragment() {
        super();
    }

    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        mPhoneNumber = CredsUtil.getInstance().getCurrentPhone();

        blockEdit();
        enablePhotoUpload();

        return v;
    }

    private void blockEdit(){
        mEditName.setEnabled(false);
        mEditPhone.setEnabled(false);

        mEditName.setText("You (current user)");
        mEditPhone.setText(mPhoneNumber);
    }

    private void enablePhotoUpload(){
        mButtonPhotoChange.setVisibility(View.VISIBLE);
        mButtonPhotoChange.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_photo_change:{
                EditProfileFragmentPermissionsDispatcher.pickPhotoWithPermissionCheck(this);
                break;
            }
        }
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    protected void pickPhoto(){
        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(getContext())
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        mImageAvatar.setImageURI(uri);
                        uploadPhoto(uri);
                    }
                })
                .create();

        tedBottomPicker.show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EditProfileFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    private void uploadPhoto(Uri uri){
        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference storageRef = storage.getReference();
        StorageReference photoRef = storageRef.child(CredsUtil.getInstance().getFormattedCurrentPhone() + ".jpg");

        UploadTask uploadTask = photoRef.putFile(uri);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
            }
        });
    }
}
