package com.mochat.Fragments;

import android.arch.persistence.room.InvalidationTracker;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mochat.Adapters.ContactsRecyclerViewAdapter;
import com.mochat.MochatApp;
import com.mochat.Models.Contact;
import com.mochat.Models.Message;
import com.mochat.R;
import com.mochat.Utils.AppDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ContactsFragment extends Fragment {

    private AppDatabase mDb;
    private ContactsRecyclerViewAdapter mAdapter;
    private OnContactsListFragmentInteractionListener mListener;

    private InvalidationTracker.Observer mObserver = new InvalidationTracker.Observer("chats", "messages") {
        @Override
        public void onInvalidated(@NonNull Set<String> set) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateList();
                }
            });
        }
    };

    public ContactsFragment() {
    }


    @SuppressWarnings("unused")
    public static ContactsFragment newInstance() {
        return new ContactsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDb();
    }

    private void initDb(){
        mDb = ((MochatApp)getActivity().getApplication()).getDb();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            List<Contact> contacts = mDb.getContactDao().getAllContacts();
            List<Pair<Contact, Message>> items = new ArrayList<>();
            for(Contact contact : contacts){
                Message lastMessage = mDb.getChatDao().getInnerLastMessageForChat(contact.getPhoneMain());
                items.add(new Pair<>(contact, lastMessage));
            }
            mAdapter = new ContactsRecyclerViewAdapter(getContext(), items, mListener);
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mDb.getInvalidationTracker().addObserver(mObserver);
        updateList();
    }

    @Override
    public void onPause() {
        super.onPause();
        mDb.getInvalidationTracker().removeObserver(mObserver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnContactsListFragmentInteractionListener) {
            mListener = (OnContactsListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    private void updateList(){
        List<Contact> contacts = mDb.getContactDao().getAllContacts();
        List<Pair<Contact, Message>> items = new ArrayList<>();
        for(Contact contact : contacts){
            Message lastMessage = mDb.getChatDao().getInnerLastMessageForChat(contact.getPhoneMain());
            items.add(new Pair<>(contact, lastMessage));
        }
        mAdapter.updateChats(items);
    }

    public interface OnContactsListFragmentInteractionListener {
        void onContactClick(Contact item, View sharedView);
        void onContactLongClick(Contact item, View sharedView);
    }
}
