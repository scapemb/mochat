package com.mochat.FCM;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mochat.Utils.CredsUtil;

import static com.mochat.Sync.MessageReceiver.ACTION_MESSAGE;
import static com.mochat.Sync.MessageReceiver.EXTRA_MESSAGE;
import static com.mochat.Sync.MessageReceiver.EXTRA_PHONE;

/**
 * Created by PICA on 23.10.2017.
 */

public class MochatFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFMService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        Log.d(TAG, "FCM Message Id: " + remoteMessage.getMessageId());
        Log.d(TAG, "FCM Notification Message: " +
                remoteMessage.getNotification());
        Log.d(TAG, "FCM Data Message: " + remoteMessage.getData());

        String to = remoteMessage.getNotification().getTitle();
        String message = remoteMessage.getNotification().getBody();
        String from = remoteMessage.getNotification().getTag();


        if(null != to && to.equals(CredsUtil.getInstance().getCurrentPhone())) {
            Intent i = new Intent(ACTION_MESSAGE);
            i.putExtra(EXTRA_PHONE, from);
            i.putExtra(EXTRA_MESSAGE, message);
            sendBroadcast(i);
        }
    }
}