package com.mochat.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mochat.Models.Contact;

import java.util.List;

/**
 * Created by PICA on 11.10.2017.
 */

@Dao
public interface ContactDAO {

    @Insert
    void insert(Contact contact);

    @Insert
    void insertAll(Contact... contacts);

    @Delete
    void delete(Contact contact);

    @Query("SELECT * FROM contacts")
    List<Contact> getAllContacts();

    @Query("SELECT * FROM contacts WHERE phoneMain LIKE :phoneNumber LIMIT 1")
    Contact getContactWithPhone(String phoneNumber);
}
