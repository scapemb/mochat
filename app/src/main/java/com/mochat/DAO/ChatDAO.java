package com.mochat.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mochat.Models.Chat;
import com.mochat.Models.Message;

import java.util.List;

/**
 * Created by PICA on 11.10.2017.
 */

@Dao
public interface ChatDAO {

    @Insert
    void insert(Chat chat);

    @Insert
    void insertAll(Chat... chats);

    @Insert
    void insert(Message message);

    @Insert
    void insertAll(Message... messages);

    @Delete
    void delete(Chat chat);

    @Query("SELECT * FROM chats")
    List<Chat> getAllChats();

    @Query("SELECT * FROM chats WHERE recipientPhone IS :recipientPhone LIMIT 1")
    Chat getChatForPhone(String recipientPhone);

    @Deprecated
    @Query("SELECT * FROM messages WHERE recipientPhone IS :recipientPhone")    //available with foreign keys
    List<Message> getAllMessagesForChat(String recipientPhone);

    @Query("SELECT * FROM messages " +
            "INNER JOIN chats ON chats.recipientPhone = messages.recipientPhone " +
            "WHERE chats.recipientPhone IS :recipientPhone")
    List<Message> getInnerAllMessagesForChat(String recipientPhone);

    @Query("SELECT * FROM messages " +
            "INNER JOIN chats ON chats.recipientPhone = messages.recipientPhone " +
            "WHERE chats.recipientPhone IS :recipientPhone " +
            "ORDER BY messages.timestamp DESC " +
            "LIMIT 1")
    Message getInnerLastMessageForChat(String recipientPhone);


    @Query("DELETE FROM messages " +
            "WHERE messages.id IN (" +
            "SELECT messages.id FROM messages " +
            "INNER JOIN chats ON chats.recipientPhone = messages.recipientPhone " +
            "WHERE chats.recipientPhone IS :recipientPhone )")
    void deleteAllMessagesForChat(String recipientPhone);
}
