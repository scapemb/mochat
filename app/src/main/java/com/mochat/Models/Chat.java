package com.mochat.Models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by PICA on 06.10.2017.
 */

@Entity(tableName = "chats")
public class Chat {
    @PrimaryKey
    @NonNull
    private String recipientPhone;

    public Chat(@NonNull String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    @NonNull
    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(@NonNull String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }
}
