package com.mochat.Models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by PICA on 06.10.2017.
 */

@Entity(tableName = "contacts")
public class Contact {
    @PrimaryKey
    @NonNull
    String phoneMain;
    String name;

    public Contact(String phoneMain, String name) {
        this.phoneMain = phoneMain;
        this.name = name;
    }

    public String getPhoneMain() {
        return phoneMain;
    }
    public String getFormattedPhoneMain() {
        return phoneMain.replace("+", "");
    }

    public void setPhoneMain(String phoneMain) {
        this.phoneMain = phoneMain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
