package com.mochat.Models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;

import java.util.Date;

/**
 * Created by PICA on 09.10.2017.
 */

@Entity(tableName = "messages")
public class Message {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    private String recipientPhone;
    private boolean isOutgoing;  //true: from currentUser
    private String message;
    private long timestamp;

    public Message(String recipientPhone, boolean isOutgoing, String message) {
        this.recipientPhone = recipientPhone;
        this.isOutgoing = isOutgoing;
        this.message = message;
        this.timestamp = getCurrentTimestamp();
    }

    @Ignore
    public Message(String recipientPhone, String message) {
        this.recipientPhone = recipientPhone;
        this.isOutgoing = false;
        this.message = message;
        this.timestamp = getCurrentTimestamp();
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    public boolean isOutgoing() {
        return isOutgoing;
    }

    public void setOutgoing(boolean outgoing) {
        this.isOutgoing = outgoing;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTime() {
        return  DateFormat.format("MMM d, HH:mm", new Date(this.timestamp)).toString();

    }

    private long getCurrentTimestamp(){
        return System.currentTimeMillis();
    }
}
