package com.mochat.Utils;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.mochat.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by PICA on 17.10.2017.
 */

public class NotificationUtil {

    private static int mNotificationId = 101;

    public static void buildNotification(Context context, String message){
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.placeholder_avatar)
                        .setContentTitle("MOCHA T")
                        .setContentText(message);


        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
