package com.mochat.Utils;

/**
 * Created by PICA on 23.10.2017.
 */

public class CredsUtil {
    private static CredsUtil instance = new CredsUtil();

    private CredsUtil(){}

    public static CredsUtil getInstance() {
        return instance;
    }

    private  String currentPhone;
    public  String getCurrentPhone() {
        return currentPhone;
    }
    public  String getFormattedCurrentPhone() {
        return currentPhone.replace("+", "");
    }
    public  void setCurrentPhone(String currentPhone) {
        this.currentPhone = currentPhone;
    }
}
