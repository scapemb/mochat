package com.mochat.Utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.mochat.Models.Contact;
import com.mochat.R;

/**
 * Created by PICA on 27.10.2017.
 */

public class ContactOptionsDialogFragment extends DialogFragment {

    public interface OnContactOptionsSelected{
        void onOpenInfoSelected(Contact contact);
        void onEditInfoSelected(Contact contact);
        void onClearChatSelected(Contact contact);
        void onDeleteSelected(Contact contact);
    }

    private OnContactOptionsSelected mOptionsSelectedCallback;
    private Contact mContact;

    public void setOptionsSelectedCallback(OnContactOptionsSelected optionsSelectedCallback) {
        mOptionsSelectedCallback = optionsSelectedCallback;
    }

    public void setContact(Contact contact) {
        mContact = contact;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.options_title)
                .setItems(R.array.options_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(null == mOptionsSelectedCallback || null == mContact) return;
                        switch (which){
                            case 1: {
                                mOptionsSelectedCallback.onEditInfoSelected(mContact);
                                break;
                            }
                            case 2: {
                                mOptionsSelectedCallback.onClearChatSelected(mContact);
                                break;
                            }
                            case 3: {
                                mOptionsSelectedCallback.onDeleteSelected(mContact);
                                break;
                            }
                            case 0:
                            default: {
                                mOptionsSelectedCallback.onOpenInfoSelected(mContact);
                                break;
                            }
                        }
                    }
                });
        return builder.create();
    }
}
