package com.mochat.Utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.mochat.DAO.ChatDAO;
import com.mochat.DAO.ContactDAO;
import com.mochat.Models.Chat;
import com.mochat.Models.Contact;
import com.mochat.Models.Message;

/**
 * Created by PICA on 11.10.2017.
 */

@Database(entities = {Contact.class, Chat.class, Message.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{
    public abstract ContactDAO getContactDao();
    public abstract ChatDAO getChatDao();
}
