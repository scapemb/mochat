package com.mochat;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.mochat.Utils.AppDatabase;

/**
 * Created by PICA on 11.10.2017.
 */

public class MochatApp extends Application {
    AppDatabase mDatabase;
    public static final String BASE_URL = "https://firebasestorage.googleapis.com/v0/b/mochat-b3add.appspot.com/o/%s.jpg?alt=media";

    @Override
    public void onCreate() {
        super.onCreate();
        buildDb();
    }

    private void buildDb(){
        mDatabase = Room.databaseBuilder(
                getApplicationContext(),
                AppDatabase.class,
                "contacts_db.db")
        .allowMainThreadQueries()
        .build();
    }

    public AppDatabase getDb() {
        return mDatabase;
    }
}
