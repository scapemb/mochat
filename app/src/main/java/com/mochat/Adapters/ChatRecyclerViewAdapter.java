package com.mochat.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mochat.MochatApp;
import com.mochat.Models.Contact;
import com.mochat.Models.Message;
import com.mochat.R;
import com.mochat.Utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<ChatRecyclerViewAdapter.ViewHolder> {

    private Contact mContact;
    private ArrayList<Message> mMessages;
    private Context mContext;

    private float mFontSize = 0;

    public ChatRecyclerViewAdapter(Context context) {
        mMessages = new ArrayList<>();
        this.mContext = context;
    }

    public void setMessages(List<Message> messages) {
        mMessages.clear();
        mMessages.addAll(messages);
    }

    public void setTextSize(float size){
        this.mFontSize = size + 16;
        notifyDataSetChanged();
    }

    public void setContact(Contact contact) {
        mContact = contact;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mMessage = mMessages.get(position);

        String recipientName = (null == mContact.getName() || mContact.getName().isEmpty()) ? mContact.getPhoneMain() : mContact.getName();
        holder.mNameText.setText(holder.mMessage.isOutgoing() ? "You" : recipientName);
        holder.mTimeText.setText(holder.mMessage.getTime());
        holder.mMessageText.setText(holder.mMessage.getMessage());

        holder.mAvatarImage.setVisibility(holder.mMessage.isOutgoing() ? View.GONE : View.VISIBLE);
        setMessageDirection(holder);

        Picasso.with(mContext)
                .load(String.format(
                        MochatApp.BASE_URL,
                        mContact.getFormattedPhoneMain()))
                .transform(new CircleTransform())
                .resize(150, 150)
                .centerCrop()
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.mAvatarImage);

        if(mFontSize != 0){
            holder.mMessageText.setTextSize(mFontSize);
            holder.mNameText.setTextSize(mFontSize - 4);
            holder.mTimeText.setTextSize(mFontSize - 6);
        }
    }

    private void setMessageDirection(final ViewHolder holder) {
        View v = holder.mView;
        View textContainer = holder.mContainerText;
        boolean isOutgoing = holder.mMessage.isOutgoing();

        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) v.getLayoutParams();
        params.setMargins(isOutgoing ? (int)getScreenWidth() : 16, 8, isOutgoing ? 16 : (int)getScreenWidth(), 8); //substitute parameters for left, top, right, bottom
        v.setLayoutParams(params);
        textContainer.setBackgroundColor(mContext.getResources().getColor(isOutgoing ? R.color.colorMessageDark : R.color.colorMessageLight));
    }

    private float getScreenWidth(){
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels / displayMetrics.density;
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout mCoreView;
        public final LinearLayout mContainerText;
        public final ImageView mAvatarImage;
        public final TextView mNameText;
        public final TextView mTimeText;
        public final TextView mMessageText;
        public Message mMessage;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCoreView = view.findViewById(R.id.core_view);
            mContainerText = view.findViewById(R.id.container_text);
            mAvatarImage = view.findViewById(R.id.avatar);
            mNameText = view.findViewById(R.id.name);
            mTimeText = view.findViewById(R.id.time);
            mMessageText = view.findViewById(R.id.message);
        }
    }
}
