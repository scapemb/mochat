package com.mochat.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mochat.Activities.MainActivity;
import com.mochat.Fragments.ContactsFragment.OnContactsListFragmentInteractionListener;
import com.mochat.MochatApp;
import com.mochat.Models.Contact;
import com.mochat.Models.Message;
import com.mochat.R;
import com.mochat.Utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<Pair<Contact, Message>> mValues;
    private final OnContactsListFragmentInteractionListener mListener;

    public ContactsRecyclerViewAdapter(Context context, List<Pair<Contact, Message>> items, OnContactsListFragmentInteractionListener listener) {
        mContext = context;
        mValues = items;
        mListener = listener;
    }

    public void updateChats(List<Pair<Contact, Message>> items){
        mValues.clear();
        mValues = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mContact = mValues.get(position).first;
        holder.mLastMessage = mValues.get(position).second;
        holder.mNameText.setText(holder.mContact.getName());
        holder.mPhoneText.setText(holder.mContact.getPhoneMain());
        holder.mLastMessageText.setVisibility(null == holder.mLastMessage ? View.GONE : View.VISIBLE);
        if(null != holder.mLastMessage) {
            holder.mLastMessageText.setText((holder.mLastMessage.isOutgoing() ? "You: " : holder.mContact.getName() + ": ") +
                    holder.mLastMessage.getMessage()
            );
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    holder.mAvatarImage.setTransitionName(MainActivity.ARG_SHARED_VIEW);
                    mListener.onContactClick(holder.mContact, holder.mAvatarImage);
                }
            }
        });
        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (null != mListener) {
                    holder.mAvatarImage.setTransitionName(MainActivity.ARG_SHARED_VIEW);
                    mListener.onContactLongClick(holder.mContact, holder.mAvatarImage);
                }
                return true;
            }
        });

        Picasso.with(mContext)
                .load(String.format(
                        MochatApp.BASE_URL,
                        holder.mContact.getFormattedPhoneMain()))
                .resize(150, 150)
                .centerCrop()
                .transform(new CircleTransform())
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.mAvatarImage);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mAvatarImage;
        public final TextView mNameText;
        public final TextView mPhoneText;
        public final TextView mLastMessageText;
        public Contact mContact;
        public Message mLastMessage;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mAvatarImage = view.findViewById(R.id.avatar);
            mNameText = view.findViewById(R.id.name);
            mPhoneText = view.findViewById(R.id.phone);
            mLastMessageText = view.findViewById(R.id.message_last);
        }
    }
}
