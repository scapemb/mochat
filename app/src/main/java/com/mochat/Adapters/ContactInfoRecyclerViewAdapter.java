package com.mochat.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mochat.Activities.MainActivity;
import com.mochat.MochatApp;
import com.mochat.Models.Contact;
import com.mochat.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactInfoRecyclerViewAdapter extends RecyclerView.Adapter<ContactInfoRecyclerViewAdapter.ViewHolder> {

    private final List<Contact> mValues;
    private final Context mContext;

    public ContactInfoRecyclerViewAdapter(Context context, List<Contact> items) {
        mContext = context;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_info_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mContact = mValues.get(position);
        holder.mNameText.setText(holder.mContact.getName());
        holder.mPhoneText.setText(holder.mContact.getPhoneMain());

        Picasso.with(mContext)
                .load(String.format(
                        MochatApp.BASE_URL,
                        mValues.get(position).getPhoneMain().replace("+", "")))
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.mAvatarImage);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mAvatarImage;
        public final TextView mNameText;
        public final TextView mPhoneText;
        public Contact mContact;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mAvatarImage = view.findViewById(R.id.avatar);
            mNameText = view.findViewById(R.id.name);
            mPhoneText = view.findViewById(R.id.phone);

            mAvatarImage.setTransitionName(MainActivity.ARG_SHARED_VIEW);
        }
    }
}
